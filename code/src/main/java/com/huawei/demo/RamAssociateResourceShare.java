package com.huawei.demo;

// 用户身份认证
import com.huaweicloud.sdk.core.auth.GlobalCredentials;
import com.huaweicloud.sdk.core.auth.ICredential;
// 请求异常类
import com.huaweicloud.sdk.core.exception.ClientRequestException;
// 导入待请求接口的 request 和 response 类
import com.huaweicloud.sdk.ram.v1.RamClient;
import com.huaweicloud.sdk.ram.v1.model.SearchResourceShareAssociationsRequest;
import com.huaweicloud.sdk.ram.v1.model.SearchResourceShareAssociationsReqBody;
import com.huaweicloud.sdk.ram.v1.model.SearchResourceShareAssociationsResponse;
import com.huaweicloud.sdk.ram.v1.model.AssociateResourceShareRequest;
import com.huaweicloud.sdk.ram.v1.model.AssociateResourceShareResponse;
import com.huaweicloud.sdk.ram.v1.model.DisassociateResourceShareRequest;
import com.huaweicloud.sdk.ram.v1.model.DisassociateResourceShareResponse;
import com.huaweicloud.sdk.ram.v1.model.ResourceShareAssociationReqBody;
import com.huaweicloud.sdk.ram.v1.region.RamRegion;
// 日志打印
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class RamAssociateResourceShare {
    private static final Logger logger = LoggerFactory.getLogger(RamAssociateResourceShare.class.getName());
    public static void main(String[] args) {
        // 创建认证
        String ak = "{your ak string}";
        String sk = "{your sk string}";

        ICredential auth = new GlobalCredentials().withAk(ak).withSk(sk);

        RamClient ramClient = RamClient.newBuilder()
            .withCredential(auth)
            .withRegion(RamRegion.valueOf("cn-north-4"))
            .build();

        RamAssociateResourceShare demo = new RamAssociateResourceShare();

        // 1、绑定资源使用者和共享资源
        demo.associateResourceShare(ramClient);
        // 2、移除资源使用者和共享资源
        demo.disassociateResourceShare(ramClient);
        // 3、检索绑定的资源使用者和共享资源
        demo.searchResourceShareAssociations(ramClient);
    }

    public void associateResourceShare(RamClient ramClient) {
        AssociateResourceShareRequest request = new AssociateResourceShareRequest()
            .withResourceShareId("your resource share id");  // 资源共享实例的ID
        List<String> resourceUrns = new ArrayList<>();
        resourceUrns.add("your principals");  // 与资源共享实例关联的一个或多个资源使用者的列表
        List<String> principals = new ArrayList<>();
        principals.add("your resource urns");  // 与资源共享实例关联的一个或多个共享资源URN的列表
        ResourceShareAssociationReqBody body = new ResourceShareAssociationReqBody()
            .withResourceUrns(resourceUrns)
            .withPrincipals(principals);
        request.withBody(body);
        try {
            AssociateResourceShareResponse response = ramClient.associateResourceShare(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    public void disassociateResourceShare(RamClient ramClient) {
        DisassociateResourceShareRequest request = new DisassociateResourceShareRequest()
            .withResourceShareId("your resource share id");  // 资源共享实例的ID
        List<String> principals = new ArrayList<>();
        principals.add("your resource urns");  // 与资源共享实例关联的一个或多个共享资源URN的列表
        List<String> resourceUrns = new ArrayList<>();
        resourceUrns.add("your principals");  // 与资源共享实例关联的一个或多个资源使用者的列表
        ResourceShareAssociationReqBody body = new ResourceShareAssociationReqBody()
            .withResourceUrns(resourceUrns)
            .withPrincipals(principals);
        request.withBody(body);
        try {
            DisassociateResourceShareResponse response = ramClient.disassociateResourceShare(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    public void searchResourceShareAssociations(RamClient ramClient) {
        SearchResourceShareAssociationsRequest request = new SearchResourceShareAssociationsRequest()
            .withBody(new SearchResourceShareAssociationsReqBody()
                .withAssociationType(SearchResourceShareAssociationsReqBody
                    .AssociationTypeEnum.fromValue("principal or resource")));  // 指定绑定的类型（principal或resource）
        try {
            SearchResourceShareAssociationsResponse response = ramClient.searchResourceShareAssociations(request);
            logger.info(response.toString());
        } catch (ClientRequestException e) {
            LogError(e);
        }
    }

    private static void LogError(ClientRequestException e) {
        logger.error("HttpStatusCode: " + e.getHttpStatusCode());
        logger.error("RequestId: " + e.getRequestId());
        logger.error("ErrorCode: " + e.getErrorCode());
        logger.error("ErrorMsg: " + e.getErrorMsg());
    }
}
